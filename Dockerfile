FROM python:3.11 as builder

RUN echo "Intalling apt dependencies..." \
    && apt-get -y update \
    && apt-get -y upgrade \
    && apt-get -y install git

RUN echo "Cloning..." \
    && git clone https://github.com/0sir1ss/Anubis

RUN echo "Intalling pip dependencies..." \
    && pip install -r /Anubis/requirements.txt \
    && pip install requests

COPY app /app

# This cannot work for now
#RUN echo "Running Anubis on code..." \
#    && echo n >> args \
#    && echo n >> args \
#    && echo y >> args \
#    && echo c >> args \
#    && echo n >> args \
#    && echo n >> args \
#    && echo y >> args \
#    && cat args \
#    && echo "/app/__main__.py" | cat - args | python /Anubis/anubis.py \
#    && mv /app/__main__-obf.py /app/__main__.py

FROM python:3.11
COPY requirements.txt /req.txt
COPY --from=builder /app /app

RUN echo "Intalling dependencies..." \
    && pip install -r /req.txt \
    && rm /req.txt

RUN echo "Configuring app..." \
    && mkdir -p /output

ENV GAS_EXPOSE_HOST=0.0.0.0
ENV GAS_OUTPUT_PATH=/output/output.wav
ENV GAS_DOCKER_GREETER=true
EXPOSE 8080
VOLUME /output
HEALTHCHECK --interval=1m --timeout=3s \
  CMD curl -f http://localhost:8080/ || exit 1

ENTRYPOINT [ "python", "/app" ]
