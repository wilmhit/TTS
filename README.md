# TTS web UI

This repository contains 

## Developer environment

### Prerequisites

 - python 3.11
 - pip3

### Setup

```bash
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
python app
```

## Production environment

### Prerequisites

 - docker
 - docker compose plugin

### Setup

```bash
make
docker compose up -d
```
