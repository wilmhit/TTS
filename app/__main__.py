import cherrypy
import ebooks_client
from os import environ
from gastts import tts
import sys


class Common:
    ################################################
    # Author of this repo
    # https://github.com/botoxparty/XP.css
    # wrote in his README:
    # <meta charset="UTF-8" />
    # <!-- Windows XP Theme (include only one theme at a time) -->
    # <link rel="stylesheet" href="https://unpkg.com/xp.css" />

    head = """
        <head>
            <title>Great generator</title>
            <meta charset="UTF-8" />
            <!-- Windows XP Theme (include only one theme at a time) -->
            <link rel="stylesheet" href="https://unpkg.com/xp.css" />
        </head>
        <style> 
            html {
                background: url("https://www.technocrazed.com/wp-content/uploads/2015/12/Windows-XP-wallpaper-2.jpg") no-repeat center center fixed;
                background-size: cover;
            }
        </style>

    """

    # this hould't be necessary
    style = """
        <style> 
            html {
                background: url("https://www.technocrazed.com/wp-content/uploads/2015/12/Windows-XP-wallpaper-2.jpg") no-repeat center center fixed;
                background-size: cover;
            }
        </style>
     """

    @staticmethod
    def body(body):
        return f"""
          <body>
            <main>
                {body}
            </main>
          </body>
        """


class App(object):
    @cherrypy.expose
    def default(self):
        body = """
    <div class="window" style="margin: 32px; width: 250px">
      <div class="title-bar">
        <div class="title-bar-text">
          Generate Anonther Shitpost (GAS)
        </div>
      </div>
      <div class="window-body">
        <p>Hi there!</p>
        <p>Here the Fun begins, The special AI form the future will generate for you "completely random" text and then generate audio file on order to speek it to you!</p>
        <br/>
        <p>Now you don't have to waste time thinkig what a stupid and non sencical post sent to social media, AI will talk to you what to write... what a abeautifull times we are living in.<p/>
        <br/>
        <p>Just click the button below and have fun! Humanoids realy like to push buttons for some reaon.¯\_(ツ)_/¯</p>

        <button type="submit" onclick="window.location.href='/generate'">Generate</button>
        <button type="submit" onclick="window.location.href='/adult'">About</button>

      </div>
    </div>
        """

        return f"""
        <html>
            {Common.head}
            {Common.body(body)}
        </html>
        """

    @cherrypy.expose
    def adult(self):
        body = """

            <div class="window" style="margin: 32px; width: 250px">
            <div class="title-bar">
                <div class="title-bar-text">
                About
                </div>
            </div>
            <div class="window-body">
                <p>Hi there! This project was made by these humanoids: Jakub Chodubski, Przemysław Musiał, Jacek Kwieciński. </p>

                <p>You can chack Git repo if you don't thave interesting things to do in life... or just go back and have fun time.<p/>
                <button type="submit" onclick="window.location.href='https://codeberg.org/wilmhit/TTS'">Git</button>
                <button type="submit" onclick="window.location.href='/'">Back</button>
            </div>
        """

        return f"""
        <html>
            {Common.head}
            {Common.body(body)}
        </html>
        """

    @cherrypy.expose
    def generate(self):
        input_text = ebooks_client.get_text()
        tts(input_text)
        return f"""
        <html>
            {Common.head}
            {Common.body(f"Look for: '{input_text}' in wav file on your disk")}
        </html>
        """


if environ.get("GAS_DOCKER_GREETER"):
    print(
        """

    Halo!!1

    OwO? What's this!?

    You appear to be running GAS from a container
    This is a really cool way to do it σωσ !!! 

    Just be sure to:
     1. Mount a volume under '/output' path. You will receive files there ^_^
     2. Expose port 8080 from the container. We would love to take port 80
        on your puter but we will settle for settle for any.(U ᵕ U❁) rawr

    """
    )
sys.stdout.flush()
cherrypy.config.update(
    {"server.socket_host": environ.get("GAS_EXPOSE_HOST") or "127.0.0.1"}
)
cherrypy.quickstart(App(), "")
