import requests
from html import unescape

def get_encoded_text() -> str:
    return requests.get("https://awawa.cat/ebooks-reply").text

def get_text():
    return unescape(get_encoded_text())

