import contextlib
import sys
import os

out_path = os.environ.get("GAS_OUTPUT_PATH") or "output.wav"
use_cuda = os.environ.get("GAS_USE_CUDA") or False
pipe_out = os.environ.get("GAS_PIPE_OUT") or False
model_path = os.environ.get("GAS_MODEL_PATH") or "app/model_file.pth"

def tts(text: str):
    global pipe_out
    global use_cuda
    global out_path
    global model_path

    with contextlib.redirect_stdout(None if pipe_out else sys.stdout):
        from TTS.utils.synthesizer import Synthesizer

        device = "cuda" if use_cuda else "cpu"
        synthesizer = Synthesizer(
            model_path,
            "app/config.json",
        ).to(device)

        print(f" > Text: {text}")
        wav = synthesizer.tts(text)
        print(f" > Saving output to {out_path}")

        pipe_out = sys.stdout if pipe_out else None
        synthesizer.save_wav(wav, out_path, pipe_out=pipe_out)
